package com.applaudo.test.controller;


import com.applaudo.test.dto.ProductLikeDto;
import com.applaudo.test.dto.ProductPriceDto;
import com.applaudo.test.dto.ProductResponseDto;
import com.applaudo.test.exception.product.ProductNotFoundException;
import com.applaudo.test.model.LogPrice;
import com.applaudo.test.model.Product;
import com.applaudo.test.repository.ILogPriceRepo;
import com.applaudo.test.service.IProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.PermitAll;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.Optional;

@RestController
@RequestMapping("/product")
@Validated
public class ProductController {

    private static final Logger logger = LoggerFactory.getLogger(ProductController.class);

    @Autowired
    private IProductService iProductService;

    @Autowired
    private ILogPriceRepo iLogPriceRepo;


    @PermitAll
    @GetMapping("/list")
    public Page<Product> list(@RequestParam Optional<String> name,@RequestParam Optional<Integer> page,@RequestParam Optional<String> sort){
        return iProductService.allProductsSortedByNameAndLikes(name.orElse("_"),page.orElse(0),sort.orElse("name"));
    }

    @PostMapping("/save")
    public ResponseEntity<Object> save(@Valid @RequestBody Product product){
        ProductResponseDto productResponseDto = new ProductResponseDto();
        try {
            iProductService.save(product);
        }
        catch (Exception e){
            logger.error(e.getMessage());
            productResponseDto.setStatus(ProductResponseDto.getERROR());
            productResponseDto.setMessage(ProductResponseDto.getMESSAGE_ERROR());
            return new ResponseEntity<>(productResponseDto, HttpStatus.BAD_REQUEST);
        }
        productResponseDto.setStatus(ProductResponseDto.getOK());
        productResponseDto.setMessage(ProductResponseDto.getMESSAGE_SAVE());
        return new ResponseEntity<>(productResponseDto, HttpStatus.CREATED);
    }

    @PutMapping("/update")
    public ResponseEntity<Object> update(@Valid @RequestBody Product product,Authentication authentication){
        ProductResponseDto productResponseDto = new ProductResponseDto();
        Product productFind =iProductService.findById(product.getIdProduct()).orElseThrow(() -> new ProductNotFoundException());
        try {
            iProductService.save(product);

            if(productFind.getPrice()!=product.getPrice()) {
                LogPrice logPrice = new LogPrice();
                logPrice.setProduct(product);
                logPrice.setLastPrice(productFind.getPrice());
                logPrice.setCreatedBy(authentication.getName());
                iLogPriceRepo.save(logPrice);
            }
        }
        catch (Exception e){
            logger.error(e.getMessage());
            productResponseDto.setStatus(ProductResponseDto.getERROR());
            productResponseDto.setMessage(ProductResponseDto.getMESSAGE_ERROR());
            return new ResponseEntity<>(productResponseDto, HttpStatus.BAD_REQUEST);
        }
        productResponseDto.setStatus(ProductResponseDto.getOK());
        productResponseDto.setMessage(ProductResponseDto.getMESSAGE_UPD());
        return new ResponseEntity<>(productResponseDto, HttpStatus.OK);
    }

    @PatchMapping("/update")
    public ResponseEntity<Object> updatePrice(@Valid @RequestBody ProductPriceDto productPriceDto, Authentication authentication){
        ProductResponseDto productResponseDto = new ProductResponseDto();
        Product product = iProductService.findById(productPriceDto.getProductId()).orElseThrow(() -> new ProductNotFoundException());
        try {
            double tempPrice = product.getPrice();

            product.setPrice(productPriceDto.getPrice());
            iProductService.save(product);

            LogPrice logPrice = new LogPrice();
            logPrice.setProduct(product);
            logPrice.setLastPrice(tempPrice);
            logPrice.setCreatedBy(authentication.getName());
            iLogPriceRepo.save(logPrice);
        }
        catch (Exception e){
            logger.error(e.getMessage());
            productResponseDto.setStatus(ProductResponseDto.getERROR());
            productResponseDto.setMessage(ProductResponseDto.getMESSAGE_ERROR());
            return new ResponseEntity<>(productResponseDto, HttpStatus.BAD_REQUEST);
        }
        productResponseDto.setStatus(ProductResponseDto.getOK());
        productResponseDto.setMessage(ProductResponseDto.getMESSAGE_UPD());
        return new ResponseEntity<>(productResponseDto, HttpStatus.OK);
    }

    @PatchMapping("/liked")
    public ResponseEntity<Object> likedProduct(@Valid @RequestBody ProductLikeDto productLikeDto){
        ProductResponseDto productResponseDto = new ProductResponseDto();
        Product product = iProductService.findById(productLikeDto.getProductId()).orElseThrow(() -> new ProductNotFoundException());
        try {

            if (!productLikeDto.isLike()) {
                if (product.getLikes() > 0) {
                    product.setLikes(product.getLikes() - 1);
                    iProductService.save(product);
                }
                productResponseDto.setStatus(ProductResponseDto.getOK());
                productResponseDto.setMessage(ProductResponseDto.getMESSAGE_DISLIKE());
                return new ResponseEntity<>(productResponseDto, HttpStatus.OK);
            }

            product.setLikes(product.getLikes() + 1);
            iProductService.save(product);
        }
        catch (Exception e){
            logger.error(e.getMessage());
            productResponseDto.setStatus(ProductResponseDto.getERROR());
            productResponseDto.setMessage(ProductResponseDto.getMESSAGE_ERROR());
            return new ResponseEntity<>(productResponseDto, HttpStatus.BAD_REQUEST);
        }
        productResponseDto.setStatus(ProductResponseDto.getOK());
        productResponseDto.setMessage(ProductResponseDto.getMESSAGE_LIKE());
        return new ResponseEntity<>(productResponseDto, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<Object> delete(@Min(1) @PathVariable("id") Integer id){
        ProductResponseDto productResponseDto = new ProductResponseDto();
        Product product = iProductService.findById(id).orElseThrow(() -> new ProductNotFoundException());
        try {
            iProductService.delete(id);
        }
        catch (Exception e){
            logger.error(e.getMessage());
            productResponseDto.setStatus(ProductResponseDto.getERROR());
            productResponseDto.setMessage(ProductResponseDto.getMESSAGE_NOTDELETE());
            return new ResponseEntity<>(productResponseDto, HttpStatus.BAD_REQUEST);
        }
        productResponseDto.setStatus(ProductResponseDto.getOK());
        productResponseDto.setMessage(ProductResponseDto.getMESSAGE_DELETE());
        return new ResponseEntity<>(productResponseDto, HttpStatus.OK);
    }
}
