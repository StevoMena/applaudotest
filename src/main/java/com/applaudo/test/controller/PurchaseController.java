package com.applaudo.test.controller;


import com.applaudo.test.dto.ProductResponseDto;
import com.applaudo.test.dto.PurchaseDto;
import com.applaudo.test.dto.PurchaseResponseDto;
import com.applaudo.test.exception.product.ProductNotFoundException;
import com.applaudo.test.exception.purchase.PurchaseQuantityException;
import com.applaudo.test.model.DetailPurchase;
import com.applaudo.test.model.Product;
import com.applaudo.test.model.Purchase;
import com.applaudo.test.repository.IDetailPurchaseRepo;
import com.applaudo.test.service.IDetailPurchaseService;
import com.applaudo.test.service.IProductService;
import com.applaudo.test.service.IPurchaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.management.RuntimeOperationsException;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/purchase")
@Validated
public class PurchaseController {

    private static final Logger logger = LoggerFactory.getLogger(ProductController.class);

    @Autowired
    private IPurchaseService iPurchaseService;

    @Autowired
    private IDetailPurchaseService iDetailPurchaseService;

    @Autowired
    private IProductService iProductService;

    @PostMapping("/save")
    public ResponseEntity<Object> save(@Valid @RequestBody PurchaseDto purchaseDto, Authentication authentication){
        PurchaseResponseDto purchaseResponseDto = new PurchaseResponseDto();
        Product product = iProductService.findById(purchaseDto.getProductId()).orElseThrow(() -> new ProductNotFoundException());
        if (purchaseDto.getQuantity() > product.getStock()) throw new PurchaseQuantityException();

        try {
            Purchase purchase = new Purchase();
            purchase.setPurchaser(authentication.getName());
            iPurchaseService.save(purchase);


            DetailPurchase detailPurchase = new DetailPurchase();
            detailPurchase.setPurchase(purchase);
            detailPurchase.setProduct(product);
            detailPurchase.setPrice(product.getPrice());
            detailPurchase.setQuantity(purchaseDto.getQuantity());
            iDetailPurchaseService.save(detailPurchase);

            product.setStock(product.getStock() - purchaseDto.getQuantity());
            iProductService.update(product);
        }
        catch (Exception e){
            logger.error(e.getMessage());
            purchaseResponseDto.setStatus(PurchaseResponseDto.getERROR());
            purchaseResponseDto.setMessage(PurchaseResponseDto.getMESSAGE_ERROR());
            return new ResponseEntity<>(purchaseResponseDto, HttpStatus.BAD_REQUEST);
        }

        purchaseResponseDto.setStatus(PurchaseResponseDto.getOK());
        purchaseResponseDto.setMessage(PurchaseResponseDto.getMESSAGE_SAVE());
        return new ResponseEntity<>(purchaseResponseDto, HttpStatus.CREATED);
    }
}
