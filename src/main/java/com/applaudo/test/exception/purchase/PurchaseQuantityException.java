package com.applaudo.test.exception.purchase;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class PurchaseQuantityException extends RuntimeException {

    public PurchaseQuantityException() {
        super("The quantity of the product it out of stock!");
    }
}
