package com.applaudo.test;

import com.applaudo.test.model.*;
import com.applaudo.test.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;



@SpringBootApplication
@EntityScan(basePackages = "com.applaudo.test.model")
public class TestApplication{

    @Autowired
    private ILogPriceRepo ILogPriceRepo;

    @Autowired
    private IProductRepo IProductRepo;

    @Autowired
    private IPurchaseRepo iPurchaseRepo;

    @Autowired
    private IDetailPurchaseRepo iDetailPurchaseRepo;

    public static void main(String[] args) {

        SpringApplication.run(TestApplication.class, args);
    }

}
