package com.applaudo.test.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class PurchaseResponseDto {

    @Getter
    private static final String OK = "OK";
    @Getter
    private static final String MESSAGE_SAVE = "Purchase has been successful!";
    @Getter
    private static final String ERROR = "ERROR";
    @Getter
    private static final String MESSAGE_ERROR = "Purchase can't save!";


    private String status;
    private String message;

}
