package com.applaudo.test.dto;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;


@Data
public class PurchaseDto {

    @Positive(message="The productId must be greater than 0")
    @NotNull(message = "The productId is required")
    private int productId;

    @Positive(message = "The quantity must be greater than 0")
    @NotNull(message = "The quantity of product is required")
    private int quantity;
}
