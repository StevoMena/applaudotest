package com.applaudo.test.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Getter
@Setter
public class LogPriceDto {

    @Positive(message="The price must be greater than 0")
    @NotNull
    private int productId;

    @Positive(message="The price must be greater than 0")
    @Digits(integer = 10, fraction = 2)
    @DecimalMin(value = "0.1", message = "The price only")
    @NotNull
    private double lastPrice;
}
