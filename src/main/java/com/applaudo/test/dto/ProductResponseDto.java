package com.applaudo.test.dto;


import lombok.*;

@NoArgsConstructor
@Data
public class ProductResponseDto {
    @Getter
    private static final String OK = "OK";
    @Getter
    private static final String MESSAGE_SAVE = "Product save successfully!";
    @Getter
    private static final String ERROR = "ERROR";
    @Getter
    private static final String MESSAGE_ERROR = "Product can't save!";
    @Getter
    private static final String MESSAGE_UPD = "Product updated successfully!";
    @Getter
    private static final String MESSAGE_DISLIKE = "Product is dislike!";
    @Getter
    private static final String MESSAGE_LIKE = "Product is like!";
    @Getter
    private static final String MESSAGE_DELETE = "Product was deleted!";
    @Getter
    private static final String MESSAGE_NOTDELETE = "Product can't delete!";


    private String status;
    private String message;
}
