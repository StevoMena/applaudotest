package com.applaudo.test.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.*;

@Setter
@Getter
public class ProductLikeDto {

    @Positive(message="The productId must be greater than 0")
    @NotNull(message = "The productId of product is required")
    private int productId;

    @NotNull(message = "The like of product is required")
    private boolean like;

}
