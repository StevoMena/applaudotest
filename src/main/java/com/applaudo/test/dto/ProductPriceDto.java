package com.applaudo.test.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;


@Getter
@Setter
public class ProductPriceDto {

    @NotNull(message = "The productId of product is required")
    private int productId;

    @NotNull(message = "The price of product is required")
    @Digits(integer = 10, fraction = 2)
    @DecimalMin(value = "0.1", message = "The price must be greater than or equal to 0.1 and is required")
    private double price;

}
