package com.applaudo.test.service;

import com.applaudo.test.model.DetailPurchase;

public interface IDetailPurchaseService {

    void save(DetailPurchase detailPurchase);
}
