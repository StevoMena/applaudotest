package com.applaudo.test.service;

import com.applaudo.test.model.LogPrice;

public interface ILogPriceService {

    void save(LogPrice logPrice);
}
