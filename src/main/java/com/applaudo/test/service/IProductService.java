package com.applaudo.test.service;

import com.applaudo.test.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;
import java.util.Optional;


public interface IProductService {

    void save(Product product);
    List<Product> list();
    void delete(int idProduct);
    void update(Product product);
    Optional<Product> findById(int idProduct);
    Page<Product> allProductsSortedByNameAndLikes(String name, int page, String sort);

}
