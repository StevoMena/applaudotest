package com.applaudo.test.service.impl;

import com.applaudo.test.model.DetailPurchase;
import com.applaudo.test.repository.IDetailPurchaseRepo;
import com.applaudo.test.service.IDetailPurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DetailPurchaseServiceImpl implements IDetailPurchaseService {

    @Autowired
    private IDetailPurchaseRepo iDetailPurchaseRepo;

    @Override
    public void save(DetailPurchase detailPurchase) {
        iDetailPurchaseRepo.save(detailPurchase);
    }
}
