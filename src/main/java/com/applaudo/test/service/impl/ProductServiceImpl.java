package com.applaudo.test.service.impl;

import com.applaudo.test.model.Product;
import com.applaudo.test.repository.IProductRepo;
import com.applaudo.test.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements IProductService {

    @Autowired
    private IProductRepo iProductRepo;

    @Override
    public void save(Product product) {
        iProductRepo.save(product);
    }

    @Override
    public List<Product> list() {
        return iProductRepo.findAll();
    }

    @Override
    public void delete(int idProduct) {
       iProductRepo.deleteById(idProduct);
    }

    @Override
    public void update(Product product) {
        iProductRepo.save(product);
    }

    @Override
    public Optional<Product> findById(int idProduct) {
        return iProductRepo.findById(idProduct);
    }

    @Override
    public Page<Product> allProductsSortedByNameAndLikes(String name, int page, String sort) {
        Pageable pageable = PageRequest.of(page, 5, Sort.by((sort.equals("name"))?Sort.Direction.ASC :Sort.Direction.DESC,sort));
        Page<Product> allProductsSortedByName = iProductRepo.findByName(name, pageable);
        return allProductsSortedByName;
    }
}
