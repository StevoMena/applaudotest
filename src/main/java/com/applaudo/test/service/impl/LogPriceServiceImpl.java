package com.applaudo.test.service.impl;

import com.applaudo.test.model.LogPrice;
import com.applaudo.test.repository.ILogPriceRepo;
import com.applaudo.test.service.ILogPriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LogPriceServiceImpl  implements ILogPriceService {

    @Autowired
    private ILogPriceRepo iLogPriceRepo;

    @Override
    public void save(LogPrice logPrice) {
        iLogPriceRepo.save(logPrice);
    }


}
