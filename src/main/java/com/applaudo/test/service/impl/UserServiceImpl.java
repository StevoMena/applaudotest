package com.applaudo.test.service.impl;

import com.applaudo.test.model.Role;
import com.applaudo.test.model.User;
import com.applaudo.test.repository.IRoleRepo;
import com.applaudo.test.repository.IUserRepo;
import com.applaudo.test.service.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.LogManager;

@Service
public class UserServiceImpl implements UserDetailsService {


    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private IUserRepo iUserRepo;

    @Autowired
    private IRoleRepo iRoleRepo;

    @Autowired
    private BCryptPasswordEncoder encoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            User user = iUserRepo.findByUsername(username);
            if (user == null) {
                LOGGER.debug("User not found with the provided username");
                return null;
            }
            LOGGER.debug(" User from username " + user.toString());
            return new org.springframework.security.core.userdetails.User(user.getUsername(),user.getPassword(),getAuthorities(user));

        }
        catch (Exception e){
            throw new UsernameNotFoundException("User not found");
        }
    }

    private Set<GrantedAuthority> getAuthorities(User user){
        Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
        for(Role role : user.getRoles()) {
            GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(role.getRole());
            authorities.add(grantedAuthority);
        }
        LOGGER.debug("user authorities are " + authorities.toString());
        return authorities;
    }


    @PostConstruct
    public void loadUsers() {

        //Set<Role> roleAdmin = new HashSet<>(iRoleRepo.findAll());
        Set<Role> roleAdmin = new HashSet<>();
        roleAdmin.add(new Role("ADMIN"));

        Set<Role> roleUser = new HashSet<>();
        roleUser.add(new Role("USER"));

        User userAdmin = new User();
        userAdmin.setUsername("admin");
        userAdmin.setPassword(encoder.encode("root"));
        userAdmin.setRoles(roleAdmin);
        iUserRepo.save(userAdmin);

        User user = new User();
        user.setUsername("user");
        user.setPassword(encoder.encode("12345"));
        user.setRoles(roleUser);
        iUserRepo.save(user);

    }
}
