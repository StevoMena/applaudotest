package com.applaudo.test.service.impl;

import com.applaudo.test.model.Purchase;
import com.applaudo.test.repository.IPurchaseRepo;
import com.applaudo.test.service.IPurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PurchaseServiceImpl implements IPurchaseService {

    @Autowired
    private IPurchaseRepo iPurchaseRepo;

    @Override
    public void save(Purchase purchase) {
        iPurchaseRepo.save(purchase);
    }
}
