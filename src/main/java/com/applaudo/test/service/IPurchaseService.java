package com.applaudo.test.service;

import com.applaudo.test.model.Purchase;

public interface IPurchaseService {

    void save(Purchase purchase);

}
