package com.applaudo.test.service;

import com.applaudo.test.model.User;

public interface IUserService {

    User findUserByEmail(String email) ;
    User saveUser(User user);
}
