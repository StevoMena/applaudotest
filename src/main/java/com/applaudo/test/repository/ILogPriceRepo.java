package com.applaudo.test.repository;

import com.applaudo.test.model.LogPrice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ILogPriceRepo extends JpaRepository<LogPrice,Integer> {

}
