package com.applaudo.test.repository;

import com.applaudo.test.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

@Repository
public interface IProductRepo extends JpaRepository<Product,Integer> {

    @Query("select p from products p where lower(p.name) like lower(concat('%', ?1,'%'))")
    Page<Product> findByName(String name, Pageable pageable);
}
