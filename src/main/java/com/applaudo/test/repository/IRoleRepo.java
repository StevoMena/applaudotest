package com.applaudo.test.repository;

import com.applaudo.test.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IRoleRepo extends JpaRepository<Role,Integer> {
    Role findByRole(String role);
}
