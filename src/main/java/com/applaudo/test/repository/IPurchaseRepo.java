package com.applaudo.test.repository;

import com.applaudo.test.model.Purchase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IPurchaseRepo extends JpaRepository<Purchase,Integer> {
}
