package com.applaudo.test.repository;

import com.applaudo.test.model.DetailPurchase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IDetailPurchaseRepo extends JpaRepository<DetailPurchase, Integer> {
}
