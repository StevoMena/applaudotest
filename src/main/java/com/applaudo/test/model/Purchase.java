package com.applaudo.test.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Setter
@Getter
@Entity
public class Purchase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idPurchase;

    @CreationTimestamp
    private Date createdAt;

    @Column(name = "purchaser", length = 100)
    private String purchaser;

    @OneToMany(mappedBy = "purchase")
    private Set<DetailPurchase> detailPurchase;


}
