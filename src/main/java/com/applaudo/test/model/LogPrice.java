package com.applaudo.test.model;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;


@Getter
@Setter
@Entity
@Table(name = "logprices")
public class LogPrice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idLogPrice;

    @ManyToOne
    @JoinColumn(name = "productId", referencedColumnName = "idProduct")
    private Product product;

    @NotNull
    private double lastPrice;

    @CreationTimestamp
    private Date createdAt;

    @Column(name = "createdBy", nullable = false, length = 100)
    private String createdBy;


}
