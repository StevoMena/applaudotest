package com.applaudo.test.model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Setter
@Getter
@Entity
public class DetailPurchase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idDetailPurchase;

    @ManyToOne
    @JoinColumn(name = "purchaseId", referencedColumnName = "idPurchase")
    private Purchase purchase;

    @ManyToOne
    @JoinColumn(name = "productId", referencedColumnName = "idProduct")
    private Product product;

    @Positive
    @NotNull
    private int quantity;

    @Positive
    @NotNull
    private double price;


}
