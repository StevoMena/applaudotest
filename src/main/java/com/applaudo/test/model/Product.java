package com.applaudo.test.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Set;

@Getter
@Setter
@Entity(name = "products")
@Table(name = "products")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idProduct;

    @Column(name = "name", length = 200)
    @NotEmpty(message = "Please provide a product name")
    private String name;

    @Positive(message="The price must be greater than 0")
    @DecimalMin(value = "0.1", message = "The min value of price is 0.1")
    @NotNull
    private double price;

    @NotNull(message = "The stock is required")
    @PositiveOrZero(message="The stock must be >= 0")
    private int stock;

    @Column(name = "likes", nullable = false, columnDefinition = "integer default 0")
    @NotNull
    private int likes;

    @OneToMany(mappedBy="product",cascade = CascadeType.PERSIST)
    @JsonIgnore
    private Set<DetailPurchase> detailPurchaseList;

    @OneToMany(mappedBy="product",cascade = CascadeType.PERSIST)
    @JsonIgnore
    private Set<LogPrice> logPrices;


}
