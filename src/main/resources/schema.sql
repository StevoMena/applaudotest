create table detail_purchase (
	id_detail_purchase INT IDENTITY, 
	price double not null, 
	quantity INT not null, 
	product_id INT, 
	purchase_id INT, 
	primary key (id_detail_purchase)
);

create table logprices (
	id_log_price INT IDENTITY,
	product_id INT,
	last_price double not null,
	created_at timestamp,
	created_by varchar (100),
	primary key (id_log_price)
);


create table products (
	id_product INT IDENTITY, 
	likes INT default 0 not null, 
	name varchar(200) not null, 
	price double not null, 
	stock INT not null, 
	primary key (id_product)
);


create table purchase (
	id_purchase INT IDENTITY, 
	created_at timestamp, 
	purchaser varchar(150), 
	primary key (id_purchase)
);

create table roles (
	id_role INT IDENTITY, 
	role varchar(15) not null unique,
	primary key (id_role)
);


create table users (
	id_user INT IDENTITY, 
	password varchar(255) not null, 
	username varchar(100) not null, 
	primary key (id_user)
);

create table users_roles (
	user_id INT not null, 
	role_id INT not null, 
	primary key (user_id, role_id)
);

alter table users add constraint UK_r43af9ap4edm43mmtq01oddj6 unique (username);
alter table detail_purchase add constraint FKehvdlgbqfdeyv6puqg655yfp4 foreign key (product_id) references products;
alter table detail_purchase add constraint FKqbg80p05f6xivcx5psyub81ev foreign key (purchase_id) references purchase;
alter table logprices add constraint FK73ikxfs9drhbah1uv1nk1gro1 foreign key (product_id) references products;
alter table users_roles add constraint FKj6m8fwv7oqv74fcehir1a9ffy foreign key (role_id) references roles;
alter table users_roles add constraint FK2o0jvgh89lemvvo17cbqvdxaa foreign key (user_id) references users;

