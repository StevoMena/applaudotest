**Description:**
This is a Spring Boot project, build with Java 8 and Maven.

**Also the project use these packages and libraries:

1. Lombok (Integrated with project is used to minimize boilerplate code) 
2. Spring Security (Integrated with project to handle Authentication and Authorization of endpoints)
3. H2 Database (Integrated with project because is a Database Embedded, Not need Database server)
4. Actuator (Integrated with project because, you can manage and monitor your application using HTTP endpoints.)

**Before you deploy application you need install:
1. jre or jdk 8
2. maven version 3

Then set as ENV Variable Maven and Java

**If you want to deploy this projects from IDE follow these Steps:

1. Clone the project
2. Open the project with any IDE, I recommend use Intellij IDEA!
3. Install Lombok Plugin. This guide https://www.baeldung.com/lombok-ide explain how install the plugin on Intellij and Eclipse (The same steps for Eclipse are useful for STS)
4. Import Postman Collection File on POSTMAN, It's inside of project with name: "ApplaudoTest.postman_collection.json"
5. Compile the project with maven: run mvn clean install
6. Start application, always with maven mvn spring-boot:run

**If you want to deploy this project from console follow these steps:

1. Clone the project 
2. Import Postman Collection File on POSTMAN, It's inside of project with name: "ApplaudoTest.postman_collection.json"
5. Go inside carpet (../applaudotest) project and compile the project with maven run: mvn clean install
6. Start application: java -jar target\test-0.0.1-SNAPSHOT.jar 

**If you want to deploy this project with docker follow these steps:
1. Pull Image docker pull stevomena/applaudotest:1.0-SNAPSHOT (This image is public on hub.docker.com)
2. Run the image with docker run

**If you want use another Database Engine, you could find sql files on this direction :applaudotest/src/main/resources
1. schema.sql (Tables)
2. data.sql (Insert to populate Database)

**Good luck for me in this test! =)

